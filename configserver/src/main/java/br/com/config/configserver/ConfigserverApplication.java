package br.com.config.configserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigserverApplication implements CommandLineRunner {
 						// CommandLineRunner executa algo no começo da inicialização do projeto | implementa método run

	@Value("${spring.cloud.config.server.git.username}")
	private String username;

	public static void main(String[] args) {
		SpringApplication.run(ConfigserverApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//System.out.println("USERNAME = " + username);
	}
}
