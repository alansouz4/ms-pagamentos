//package com.original.apiservicepayment.dataprovider;
//
//import com.original.apiservicepayment.core.entity.CardEntity;
//import com.original.apiservicepayment.dataprovider.repositories.CardRepository;
//import com.original.apiservicepayment.core.CardEnum;
//import org.junit.Test;
//import org.junit.jupiter.api.Assertions;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.time.LocalDateTime;
//
//@RunWith(SpringRunner.class)
//public class CardDataProviderTest {
//
//    @TestConfiguration
//    static class CardDataProviderTestConfiguration{
//
//        @Bean
//        public CardDataProvider cardDataProvider(){
//            return new CardDataProvider();
//        }
//    }
//
//    @Autowired
//    CardDataProvider cardDataProvider;
//
//    @MockBean
//    CardRepository cardRepository;
//
////    @Before
////    public void setUp(){
////        CardEntity cardEntityDB = new CardEntity(1, "4558747894", "Alan", true, LocalDateTime.now(), LocalDateTime.now());
////        CardTable cardTable = CardTableMapper.toTransitory(cardEntityDB);
////        Mockito.when(cardRepository.save(cardTable))
////                .thenReturn(cardTable);
////    }
//
//    @Test
//    public void customerTestDataProviderSave(){
//
//        CardEntity cardEntity = new CardEntity(1, "4558747894", "Alan", true, LocalDateTime.now(), LocalDateTime.now(), CardEnum.BLACK);
//        cardDataProvider.create(cardEntity);
//
//        Assertions.assertEquals(cardEntity, cardEntity);
//    }
//
//}
//
