package com.original.apiservicepayment.event;

import javax.servlet.http.HttpServletResponse;

/*
    Recurso genérico, que é extendido por todos os outros recursos.
 */
public class ResourceCreatedEvent<K, T> {

    private K Long;
    private T recurso;
    private HttpServletResponse response;

    public ResourceCreatedEvent(T recurso) {
        this.recurso = recurso;
    }

    public ResourceCreatedEvent(K Long, T recurso, HttpServletResponse response) {
        this.Long = Long;
        this.recurso = recurso;
        this.response = response;
    }

    public K getId() {
        return Long;
    }

    public void setId(K id) {
        this.Long = id;
    }

    public T getRecurso() {
        return recurso;
    }

    public void setRecurso(T recurso) {
        this.recurso = recurso;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
}
