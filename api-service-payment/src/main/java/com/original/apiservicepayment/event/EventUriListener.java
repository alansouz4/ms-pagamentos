package com.original.apiservicepayment.event;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Component
public class EventUriListener<K, T> {

    @EventListener
    public void addHeaderLocation(ResourceCreatedEvent<K, T> event) {

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
                .buildAndExpand(event.getId()).toUri();

        event.getResponse().setHeader("Location", uri.toASCIIString());
    }
}
