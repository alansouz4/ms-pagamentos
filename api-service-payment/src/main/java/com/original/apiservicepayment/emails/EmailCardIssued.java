package com.original.apiservicepayment.emails;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailCardIssued {

    private static final Logger logger = LoggerFactory.getLogger(EmailCardIssued.class.getName());

    public void enviarEmailEmitido(CartaoEntity cartaoEntity) {

        AuthenticationModel auth = new AuthenticationModel();

        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(auth.getMyEmail(), auth.getPassword()));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(auth.getMyEmail());
            email.setSubject("Cartão Emitido");
            email.setMsg("Seu cartão foi emitido e encaminhado para transporte.");
            email.addTo(auth.getEmailDest());
            email.send();
            logger.info("Email enviado com sucesso para: " + auth.getEmailDest());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
