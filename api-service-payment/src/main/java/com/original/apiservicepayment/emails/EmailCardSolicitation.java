package com.original.apiservicepayment.emails;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailCardSolicitation {

    private static final Logger logger = LoggerFactory.getLogger(EmailCardSolicitation.class.getName());

    public void executaEnvioEmailSemImpacto(CartaoEntity cartaoEntity){
        new Thread(() -> {
            enviarEmailCardSolicitado(cartaoEntity);
        }).start();
    }

    public void enviarEmailCardSolicitado(CartaoEntity cartaoEntity) {

        AuthenticationModel auth = new AuthenticationModel();

        SimpleEmail email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(auth.getMyEmail(), auth.getPassword()));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(auth.getMyEmail());
            email.setSubject("Cartão Solicitado");
            email.setMsg("Sua solicitação de criação do seu cartão esta em andamento.");
            email.addTo(auth.getEmailDest());
            email.send();
            logger.info("Email enviado com sucesso para: " + auth.getEmailDest());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
