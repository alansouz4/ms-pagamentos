package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObterCardByIdUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public ObterCardByIdUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    public CartaoEntity getById(Long id) {
        return cartaoGateway.getById(id);
    }
}
