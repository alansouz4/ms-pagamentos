package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.stereotype.Component;

@Component
public class AtivarCardUseCase {

    private CartaoGateway cartaoGateway;

    public AtivarCardUseCase(CartaoGateway cartaoGateway) {
        this.cartaoGateway = cartaoGateway;
    }

    public CartaoEntity cardActive(String numero) {
        return cartaoGateway.cardActive(numero);
    }
}
