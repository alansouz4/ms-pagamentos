package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObterCartaoPorNumeroUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public ObterCartaoPorNumeroUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    public CartaoEntity getByNumber(String numero) {
        return cartaoGateway.getByNumber(numero);
    }
}
