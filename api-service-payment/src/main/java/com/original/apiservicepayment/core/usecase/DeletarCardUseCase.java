package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeletarCardUseCase {

    private CartaoGateway cartaoGateway;

    @Autowired
    public DeletarCardUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    public void delete(Long id) {
        cartaoGateway.delete(id);
    }
}
