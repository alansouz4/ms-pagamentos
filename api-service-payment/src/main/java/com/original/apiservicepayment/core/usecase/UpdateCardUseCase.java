package com.original.apiservicepayment.core.usecase;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateCardUseCase {

    public CartaoGateway cartaoGateway;

    @Autowired
    public UpdateCardUseCase(CartaoGateway cartaoGateway){
        this.cartaoGateway = cartaoGateway;
    }

    public CartaoEntity upDate(CartaoEntity cartaoEntity, Long id) {
        return cartaoGateway.upDate(cartaoEntity, id);
    }
}
