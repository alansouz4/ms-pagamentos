package com.original.apiservicepayment.core.gateway;

import com.original.apiservicepayment.core.entity.CartaoEntity;

import java.util.List;

public interface CartaoGateway {

    CartaoEntity salvarCartao(CartaoEntity cartaoEntity);
    CartaoEntity cardActive(String numero);
    CartaoEntity getByNumber(String numero);
    CartaoEntity getById(Long id);
    List<CartaoEntity> getAll(Integer offset, Integer limit);
    CartaoEntity upDate(CartaoEntity cartaoEntity, Long id);
    String delete(Long id);
}
