package com.original.apiservicepayment.core.entity;

import com.original.apiservicepayment.core.CartaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartaoEntity {

    private Long id;
    private String numero;
    private Boolean ativado;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraAlteracao;
    private CartaoEnum tipo;
    private Long idCliente;
}
