package com.original.apiservicepayment.dataprovider.entity;

import com.original.apiservicepayment.core.CartaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TB_CARTAO")
public class CartaoTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "ATIVADO")
    private Boolean ativado;

    @Column(name = "DATA_HORA_CRIACAO")
    private LocalDateTime dataHoraCriacao;

    @Column(name = "DATA_HORA_ALTERACAO")
    private LocalDateTime dataHoraAlteracao;

    @Column(name = "TIPO")
    private CartaoEnum tipo;

    @Column(name = "ID_CLIENTE")
    private Long idCliente;
}
