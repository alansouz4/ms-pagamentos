package com.original.apiservicepayment.dataprovider;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.gateway.CartaoGateway;
import com.original.apiservicepayment.dataprovider.clients.ClienteFeignClient;
import com.original.apiservicepayment.dataprovider.entity.CartaoTable;
import com.original.apiservicepayment.dataprovider.entity.Cliente;
import com.original.apiservicepayment.dataprovider.exception.CardNotFoundException;
import com.original.apiservicepayment.dataprovider.repositories.CartaoRepository;
import com.original.apiservicepayment.dataprovider.mapper.CardTableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.original.apiservicepayment.util.LogMessageUtils.*;

@Component
public class CartaoDataProvider implements CartaoGateway {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartaoDataProvider.class);

    private CartaoRepository cartaoRepository;

    private ClienteFeignClient clienteFeignClient;

    @Autowired
    public CartaoDataProvider(CartaoRepository cartaoRepository, ClienteFeignClient clienteFeignClient) {
        this.cartaoRepository = cartaoRepository;
        this.clienteFeignClient = clienteFeignClient;
    }

    @HystrixCommand(fallbackMethod = "salvarCartaoAlternativo")
    @Override
    public CartaoEntity salvarCartao(CartaoEntity cartaoEntity) {
        CartaoTable cartaoTable;
        Cliente cliente;
        try {
            LOGGER.info("[CARTÃO] | Cria Cartão - Inicio");
            cartaoTable = CardTableMapper.toTable(cartaoEntity);
            cliente = clienteFeignClient.obterClientePorId(cartaoTable.getIdCliente()).getBody();
            if (cliente.equals(null)){
                throw new RuntimeException("[CLIENTE] | Cliente não encontrado!");
            }
            if (cartaoRepository.findByNumero(cartaoTable.getNumero()).isPresent()){
                throw new RuntimeException("[CARTÃO] | Número cartão já existente!");
            }
            cartaoTable.setIdCliente(cliente.getId());
        } catch (Exception e) {
            LOGGER.error(String.format("[CARTÃO] | Erro ao criar cartão - Erro"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        LOGGER.info("[CARTÃO] | Cliente " + cartaoEntity.getIdCliente() + ", cartão Nr. " + cartaoEntity.getNumero() + " criado com sucesso - Sucesso!");
        return CardTableMapper.fromTable(cartaoRepository.save(cartaoTable));
    }

    public CartaoEntity salvarCartaoAlternativo(CartaoEntity cartaoEntity) {
        CartaoTable cartaoTable;
        Cliente cliente;
        try {
            LOGGER.info("[CARTÃO] | Cria Cartão - Inicio");
            cartaoTable = CardTableMapper.toTable(cartaoEntity);
            cliente = Cliente.builder().id(2L).build();
            if (cliente.equals(null)){
                throw new RuntimeException("[CLIENTE] | Cliente não encontrado!");
            }
            if (cartaoRepository.findByNumero(cartaoTable.getNumero()).isPresent()){
                throw new RuntimeException("[CARTÃO] | Número cartão já existente!");
            }
            cartaoTable.setIdCliente(cliente.getId());
        } catch (Exception e) {
            LOGGER.error(String.format("[CARTÃO] | Erro ao criar cartão - Erro"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        LOGGER.info("[CARTÃO] | Cliente " + cartaoEntity.getIdCliente() + ", cartão Nr. " + cartaoEntity.getNumero() + " criado com sucesso - Sucesso!");
        return CardTableMapper.fromTable(cartaoRepository.save(cartaoTable));
    }

    @Override
    public CartaoEntity cardActive(String number) {
        try {
            Optional<CartaoTable> table = cartaoRepository.findByNumero(number);
            if (table.equals(number)){
                CartaoEntity entity = CardTableMapper.fromTable(table.get());
                entity.setAtivado(true);
                LOGGER.info("[CARTÃO] | Cartão Nr. " + entity.getNumero() + " ativado com sucesso");
                return entity;
            }else {
                LOGGER.error(String.format("[CARTÃO] | Erro na ativação - Erro"));
                throw new CardNotFoundException();
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @Override
    public CartaoEntity getByNumber(String numero) {
        Optional<CartaoTable> cardTable;
        try {
            LOGGER.info("[CARTÃO] | Buscanco Cartão pelo número " + numero);
            cardTable = cartaoRepository.findByNumero(numero);
            if (!cardTable.isPresent()) {
                LOGGER.error(String.format(RETORNO_CARD_NOTFOUND));
                throw new CardNotFoundException();
            }
        } catch (Exception e) {
            LOGGER.error(String.format(RETORNO_CARD_ERRO));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        LOGGER.info(RETORNO_CARD_SUCESSO);
        return CardTableMapper.fromTable(cardTable.get());
    }

    @Override
    public CartaoEntity getById(Long id) {
        CartaoTable table;
        try {
            validationId(id);
            LOGGER.info("[CARTÃO] | Busca cartão com Id: " + id);
            table = cartaoRepository.findById(id).get();
            LOGGER.info("[CARTÃO] | Retorno com sucesso, cartão com Id: " + id);
         } catch (Exception e) {
            LOGGER.error(String.format("[CARTÃO] | Erro no processamento do servidor"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return CardTableMapper.fromTable(table);
    }

    @Override
    public List<CartaoEntity> getAll(Integer offset, Integer limit) {
        try{
            LOGGER.info(RETORNO_CARDS_SUCESSO);
            Page<CartaoTable> page = cartaoRepository.findAll(PageRequest.of(offset, limit));
            LOGGER.info(RETORNO_CARDS_SUCESSO);
            return page.stream()
                    .map(entity -> CardTableMapper.fromTable(entity))
                    .collect(Collectors.toList());
        } catch (Exception e ){
            LOGGER.error(String.format("[CARTÃO] | Erro no processamento do servidor"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public CartaoEntity upDate(CartaoEntity cartaoEntity, Long id) {
        CartaoTable cartaoTable;
        try {
            validationId(id);
            LOGGER.info("[CARTÃO] | Atualiza cartão - Inicio");
            cartaoTable = CardTableMapper.toTable(cartaoEntity);
            cartaoTable.setDataHoraAlteracao(LocalDateTime.now());
            cartaoRepository.save(cartaoTable);

        } catch (Exception e) {
            LOGGER.error(String.format("[CARTÃO] | Erro no processamento do servidor"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        LOGGER.info("[CARTÃO] | Cartão Id: " + id + " alterado com sucesso!!");
        if (cartaoTable.getNumero() != cartaoEntity.getNumero()){
            throw new RuntimeException("Cartão não pode ser alterado");
        }
        return CardTableMapper.fromTable(cartaoTable);
    }

    @Override
    public String delete(Long id) {
        try {
            validationId(id);
            cartaoRepository.deleteById(id);
            LOGGER.info("[CARTÃO] | Cartão id " + id + " deletado com sucesso!");
            return "Cartão Id: " + id + " deletado com sucesso!";
        } catch (Exception e) {
            LOGGER.error(String.format("[Erro Card] | Erro no processamento do servidor"));
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

    public void validationId(Long id){
        if (!cartaoRepository.existsById(id)){
            LOGGER.error(String.format("[CARTÃO] | Cartão Id: " + id + " não encontrado!!"));
            throw new CardNotFoundException();
        }
    }
}
