package com.original.apiservicepayment.dataprovider.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O cliente informado é inválido")
public class InvalidClienteException extends RuntimeException{
}
