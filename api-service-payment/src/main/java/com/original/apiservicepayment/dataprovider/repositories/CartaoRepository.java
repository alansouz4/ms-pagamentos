package com.original.apiservicepayment.dataprovider.repositories;

import com.original.apiservicepayment.dataprovider.entity.CartaoTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartaoRepository extends JpaRepository <CartaoTable, Long>{

    Optional<CartaoTable> findByNumero(String number);
}
