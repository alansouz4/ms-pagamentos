package com.original.apiservicepayment.dataprovider.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteFeignClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteFeignClientDecoder();
    }
}
