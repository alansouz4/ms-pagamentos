package com.original.apiservicepayment.dataprovider.mapper;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.dataprovider.entity.CartaoTable;
import com.original.apiservicepayment.dataprovider.entity.Cliente;

public class CardTableMapper {

    private CardTableMapper() {}

    public static CartaoTable toTable(CartaoEntity cartaoEntity){

        Cliente cliente = Cliente.builder()
                .id(cartaoEntity.getIdCliente())
                .build();

        CartaoTable cartaoTable = CartaoTable
                .builder()
                .id(cartaoEntity.getId())
                .numero(cartaoEntity.getNumero())
                .ativado(cartaoEntity.getAtivado())
                .dataHoraCriacao(cartaoEntity.getDataHoraCriacao())
                .dataHoraAlteracao(cartaoEntity.getDataHoraAlteracao())
                .tipo(cartaoEntity.getTipo())
                .idCliente(cliente.getId())
                .build();
        return cartaoTable;
    }

    public static CartaoEntity fromTable(CartaoTable cartaoTable){

        Cliente cliente = Cliente.builder()
                .id(cartaoTable.getIdCliente())
                .build();

        CartaoEntity cartaoEntity = CartaoEntity
                .builder()
                .id(cartaoTable.getId())
                .numero(cartaoTable.getNumero())
                .ativado(cartaoTable.getAtivado())
                .dataHoraCriacao(cartaoTable.getDataHoraCriacao())
                .dataHoraAlteracao(cartaoTable.getDataHoraAlteracao())
                .tipo(cartaoTable.getTipo())
                .idCliente(cliente.getId())
                .build();
        return cartaoEntity;
    }
}
