package com.original.apiservicepayment.presenter.entrypoint.exceptions.handlers;

import com.original.apiservicepayment.configuration.exception.BusinessException;
import com.original.apiservicepayment.presenter.entrypoint.exceptions.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class BusinessExceptionHandler {

    public ResponseEntity<ApiError> handlerBusinessException(BusinessException e) {
        ApiError apiError = ApiError.builder()
                .code(e.getCode())
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
