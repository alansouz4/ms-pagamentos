package com.original.apiservicepayment.presenter.mappers;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.dataprovider.entity.Cliente;
import com.original.apiservicepayment.presenter.model.CartaoHttpModel;
import com.original.apiservicepayment.presenter.model.CartaoResponse;

public class CartaoHttpModelMapper {

    public static CartaoResponse toResponse(CartaoEntity cartaoEntity) {

        Cliente cliente = Cliente.builder()
                .id(cartaoEntity.getIdCliente())
                .build();

        return CartaoResponse.builder()
                .id(cartaoEntity.getId())
                .numero(cartaoEntity.getNumero())
                .dataHoraCriacao(cartaoEntity.getDataHoraCriacao())
                .dataHoraAtualizacao(cartaoEntity.getDataHoraAlteracao())
                .ativado(cartaoEntity.getAtivado())
                .tipo(cartaoEntity.getTipo())
                .idCliente(cliente.getId())
                .build();
    }

    public static CartaoEntity toEntity(CartaoHttpModel cartaoHttpModel) {

        Cliente cliente = Cliente.builder()
                .id(cartaoHttpModel.getIdCliente())
                .build();

        return CartaoEntity.builder()
                .id(cartaoHttpModel.getId())
                .numero(cartaoHttpModel.getNumero())
                .dataHoraCriacao(cartaoHttpModel.getDataHoraCriacao())
                .dataHoraAlteracao(cartaoHttpModel.getDataHoraAlteracao())
                .ativado(cartaoHttpModel.getAtivado())
                .tipo(cartaoHttpModel.getTipo())
                .idCliente(cliente.getId())
                .build();
    }
}
