package com.original.apiservicepayment.presenter.entrypoint;

import com.original.apiservicepayment.core.entity.CartaoEntity;
import com.original.apiservicepayment.core.entity.annotation.NotMonitored;
import com.original.apiservicepayment.core.usecase.*;
import com.original.apiservicepayment.event.CardCreateEvent;
import com.original.apiservicepayment.presenter.mappers.CartaoHttpModelMapper;
import com.original.apiservicepayment.presenter.model.CartaoHttpModel;
import com.original.apiservicepayment.presenter.model.CartaoResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@RefreshScope
@Api(value="API Serviço de Cartão")
@RestController
@RequestMapping("/v1/cartoes")
public class CartaoEntrypoint {

    private static final Logger logger = LoggerFactory.getLogger(CartaoEntrypoint.class);

//    @Value("${server.error.include-message}")
//    private String configMsCliente;

    private CriarCartaoUseCase criarCartaoUseCase;
    private ObterCardByIdUseCase obterCardByIdUseCase;
    private ObterCartaoPorNumeroUseCase obterCartaoPorNumeroUseCase;
    private ObterTodosCardsUseCase obterTodosCardsUseCase;
    private UpdateCardUseCase updateCardUseCase;
    private DeletarCardUseCase deletarCardUseCase;
    private AtivarCardUseCase ativarCardUseCase;
    private ApplicationEventPublisher publisher;

    @Autowired
    public CartaoEntrypoint(CriarCartaoUseCase criarCartaoUseCase,
                            ObterCardByIdUseCase obterCardByIdUseCase,
                            ObterCartaoPorNumeroUseCase obterCartaoPorNumeroUseCase,
                            ObterTodosCardsUseCase obterTodosCardsUseCase,
                            UpdateCardUseCase updateCardUseCase,
                            DeletarCardUseCase deletarCardUseCase,
                            ApplicationEventPublisher publisher) {
        this.criarCartaoUseCase = criarCartaoUseCase;
        this.obterCardByIdUseCase = obterCardByIdUseCase;
        this.obterCartaoPorNumeroUseCase = obterCartaoPorNumeroUseCase;
        this.obterTodosCardsUseCase = obterTodosCardsUseCase;
        this.updateCardUseCase = updateCardUseCase;
        this.deletarCardUseCase = deletarCardUseCase;
        this.publisher = publisher;
    }

    @ApiOperation(value = "Cria um cartão")
    @PostMapping
    public ResponseEntity<Void> salvarCartao(@Validated @RequestBody CartaoHttpModel cartaoHttpModel,
                                             HttpServletResponse response) {
        CartaoEntity cartaoEntity = criarCartaoUseCase.salvarCartao(CartaoHttpModelMapper.toEntity(cartaoHttpModel));
        // Evento para enviar email
        this.publisher.publishEvent(new CardCreateEvent(cartaoEntity, response));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @NotMonitored
    @ApiOperation(value = "Obtem os cartões")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sucesso")})
    @GetMapping
    public ResponseEntity<List<CartaoResponse>> obterCartoes(@RequestParam Integer offset, @RequestParam Integer limit){
        List<CartaoEntity> cartaoEntity = obterTodosCardsUseCase.getAll(offset, limit);
        return ResponseEntity.ok().body(cartaoEntity.stream()
                        .map(entity -> CartaoHttpModelMapper.toResponse(entity))
                        .collect(Collectors.toList()));
    }

    @ApiOperation(value = "Obtem um cartão por Id")
    @GetMapping("/{id}")
    public ResponseEntity<CartaoResponse> obterCartaoPorId(@PathVariable(name = "id") Long id){
            CartaoEntity cartaoEntity = obterCardByIdUseCase.getById(id);
            if (cartaoEntity == null) {
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok().body(CartaoHttpModelMapper.toResponse(cartaoEntity));
    }

    @ApiOperation(value = "Obtem um cartão por numero")
    @GetMapping("/numero/{numero}")
    public ResponseEntity<CartaoResponse> obterCartaoPorNumero(@PathVariable(name = "numero") String numero){
        CartaoEntity cartaoEntity = obterCartaoPorNumeroUseCase.getByNumber(numero);
        if (cartaoEntity == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(CartaoHttpModelMapper.toResponse(cartaoEntity));
    }

    @ApiOperation(value = "Atualiza um cartão")
    @PutMapping("/atualizar/{id}")
    public ResponseEntity<CartaoResponse> atualizarCartao(@Validated @PathVariable(name = "id") Long id, @RequestBody CartaoHttpModel cartaoHttpModel){
        CartaoEntity cartaoEntity = obterCardByIdUseCase.getById(id);
        if (cartaoEntity == null) {
            return ResponseEntity.noContent().build();
        }
        cartaoEntity = CartaoHttpModelMapper.toEntity(cartaoHttpModel);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CartaoHttpModelMapper.toResponse(updateCardUseCase.upDate(cartaoEntity,id)));
    }

    @ApiOperation(value = "Ativar um cartão")
    @PutMapping("/ativar/{numero}")
    public ResponseEntity<String> ativarCartao(@Validated @PathVariable(name = "numero") String numero){
        obterCartaoPorNumero(numero);

        CartaoEntity cartaoEntity = ativarCardUseCase.cardActive(numero);

        return ResponseEntity.status(HttpStatus.OK)
                .body("Cartão ativado com sucesso!");
    }

    @ApiOperation(value = "Remove um cartão")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> removerCartao(@PathVariable Long id){
        if (obterCartaoPorId(id).equals(null)) {
            ResponseEntity.notFound().build();
        }
        deletarCardUseCase.delete(id);
        return ResponseEntity.status(204).body("");
    }

    // não deu certo
    @ApiOperation(value = "Obtem as configurações do config-server no repositório")
    @GetMapping("/repositorio/configs")
    public ResponseEntity<Void> obterConfiguracoesConfigServer(){
//        logger.info("CONFIG = " + configMsCliente);
        return ResponseEntity.noContent().build();
    }
}
