package com.original.apiservicepayment.util;

public class LogMessageUtils {

    public static final String RETORNO_CARD_INICIO = "[Cartão] | Buscando cartões";
    public static final String RETORNO_CARD_SUCESSO = "[Cartão] | Retorno cartões com sucesso";
    public static final String RETORNO_CARD_NOTFOUND = "[Cartão] | Erro no retorno, cartão não encontrado";
    public static final String RETORNO_CARD_ERRO = "[Cartão] | Erro no processamento do servidor";
    public static final String RETORNO_CARDS_SUCESSO = "[Cartão] | Retorno cartões com sucesso";
}
