package com.original.apiservicepayment.configuration.exception;

import java.util.ArrayList;
import java.util.List;

public class BusinessException extends RuntimeException{


    private transient List<FieldError> fields = new ArrayList<FieldError>();
    private String code;

    public BusinessException(final String code, final String message, final List<FieldError> fields) {
        super(message);
        this.code = code;
        this.fields = fields;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(final List<FieldError> fields, final String message) {
        super(message);
        this.fields = fields;
    }

    public BusinessException(String message, Throwable cause, String code) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(final String message, final List<FieldError> fields, final String code) {
        super(message);
        this.fields = fields;
        this.code = code;
    }

    public BusinessException(final String message) {
        super(message);
    }

    public BusinessException(final String message, final String code) {
        super(message);
        this.code = code;
    }

    public List<FieldError> getFields() {
        return fields;
    }

    public void setFields(List<FieldError> fields) {
        this.fields = fields;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
