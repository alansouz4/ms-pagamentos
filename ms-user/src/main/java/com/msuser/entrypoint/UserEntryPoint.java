package com.msuser.entrypoint;

import com.msuser.entities.User;
import com.msuser.repositories.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value="API Usuário")
@RestController
@RequestMapping("/v1/users")
public class UserEntryPoint {

    private UserRepository userRepository;

    @Autowired
    public UserEntryPoint(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @ApiOperation(value="Busca um usuário por id")
    @GetMapping("/{id}")
    public ResponseEntity<User> obterClientePorId(@PathVariable Long id) {
        User user = userRepository.findById(id).get();
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(user);
    }

    @ApiOperation(value="Busca um usuário por email")
    @GetMapping("/search")
    public ResponseEntity<User> obterClientePorEmail(@RequestParam String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(user);
    }

}
