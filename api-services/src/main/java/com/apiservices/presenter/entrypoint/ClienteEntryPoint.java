package com.apiservices.presenter.entrypoint;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.core.usecase.CadastroClienteUseCase;
import com.apiservices.presenter.entity.ClienteHttpModel;
import com.apiservices.presenter.entity.ClienteResponse;
import com.apiservices.presenter.mappers.ClienteHttpModelTesteMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RefreshScope
@Api(value="API Cadastro de Clientes")
@RestController
@RequestMapping("/v1/clientes")
public class ClienteEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(ClienteEntryPoint.class);

//	@Value("${server.error.include-message}")
//	private String configMsCliente;

	private CadastroClienteUseCase cadastroClienteUseCase;

	@Autowired
	public ClienteEntryPoint(CadastroClienteUseCase cadastroClienteUseCase) {
		this.cadastroClienteUseCase = cadastroClienteUseCase;
	}

	@ApiOperation(value="Lista os clientes")
	@GetMapping
	public ResponseEntity<List<ClienteResponse>> obterClientes() {
		List<ClienteEntity> clienteEntity = cadastroClienteUseCase.obterClientes();
		if (clienteEntity == null){
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok().body(clienteEntity.stream()
				.map(response -> ClienteHttpModelTesteMapper.toResponse(response)).collect(Collectors.toList()));
	}

	@ApiOperation(value="Busca um cliente")
	@GetMapping("/{id}")
	public ResponseEntity<ClienteResponse> obterClientePorId(@PathVariable Long id) {

		// espera 3 segundos pra ser processado
//		try {
//			Thread.sleep(3000L);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}

		// força exception
//		int x = 1;
//		if (x == 1)
//			throw new RuntimeException("Teste");

		ClienteEntity cliente = cadastroClienteUseCase.findById(id);
		if (cliente == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(ClienteHttpModelTesteMapper.toResponse(cliente));
	}
	
	@ApiOperation(value="Adiciona um novo cliente")
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity<Void> salvarCliente(@Valid @RequestBody ClienteHttpModel httpModel) {
		ClienteEntity entity = cadastroClienteUseCase.salvar(ClienteHttpModelTesteMapper.toEntity(httpModel));
		if (entity == null) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@ApiOperation(value="Atualiza um cliente")
	@PutMapping("/{id}")
	public ResponseEntity<ClienteResponse> atualizarCliente(@Valid @PathVariable Long id,
			@RequestBody ClienteHttpModel httpModel) {
		ClienteEntity entity = cadastroClienteUseCase.findById(id);
		if (entity == null) {
			return ResponseEntity.notFound().build();
		}
		entity = ClienteHttpModelTesteMapper.toEntity(httpModel);
		return ResponseEntity.ok(ClienteHttpModelTesteMapper.toResponse(cadastroClienteUseCase.ataulizarCliente(entity, id)));
	}

	@ApiResponses(value = {@ApiResponse(code = 204, message = "Sucesso")})
	@ApiOperation(value = "Remove um cliente")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerCliente(@PathVariable("id") Long clienteId) {
		ClienteEntity entity = cadastroClienteUseCase.findById(clienteId);
		if (entity.equals(null)) {
			return ResponseEntity.notFound().build();
		}
		cadastroClienteUseCase.excluir(clienteId);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Obtem as configurações do config-server no repositório")
	@GetMapping("/repositorio/configs")
	public ResponseEntity<Void> obterConfiguracoesConfigServer(){
//		logger.info("CONFIG = " + configMsCliente);
		return ResponseEntity.noContent().build();
	}
	
}
