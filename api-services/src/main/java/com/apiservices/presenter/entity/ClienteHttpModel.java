package com.apiservices.presenter.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClienteHttpModel {

    private Long id;
    private String nome;

    @Email
    private String email;
    private String telefone;
    private LocalDateTime dataHoraCriacao;
}
