package com.apiservices.presenter.mappers;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.presenter.entity.ClienteHttpModel;
import com.apiservices.presenter.entity.ClienteResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClienteHttpModelMapper {

    ClienteHttpModelMapper INSTANCE = Mappers.getMapper(ClienteHttpModelMapper.class);

    ClienteResponse entityToResponse(ClienteEntity clienteEntity);

    ClienteEntity entityToHttpModel(ClienteHttpModel httpModel);
}
