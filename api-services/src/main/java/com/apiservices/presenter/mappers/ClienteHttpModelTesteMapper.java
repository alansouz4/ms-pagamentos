package com.apiservices.presenter.mappers;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.presenter.entity.ClienteHttpModel;
import com.apiservices.presenter.entity.ClienteResponse;

public class ClienteHttpModelTesteMapper {

    public static ClienteEntity toEntity(ClienteHttpModel model) {

        return ClienteEntity.builder()
                .id(model.getId())
                .nome(model.getNome())
                .email(model.getEmail())
                .telefone(model.getTelefone())
                .dataHoraCriacao(model.getDataHoraCriacao())
                .build();
    }

    public static ClienteResponse toResponse(ClienteEntity entity) {
        return ClienteResponse.builder()
                .id(entity.getId())
                .nome(entity.getNome())
                .email(entity.getEmail())
                .telefone(entity.getTelefone())
                .dataHoraCriacao(entity.getDataHoraCriacao())
                .build();
    }
}
