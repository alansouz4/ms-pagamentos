package com.apiservices.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClienteEntity {

    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private LocalDateTime dataHoraCriacao;
    private LocalDateTime dataHoraAlteracao;
}
