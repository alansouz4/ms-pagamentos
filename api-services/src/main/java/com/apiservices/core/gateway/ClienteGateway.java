package com.apiservices.core.gateway;

import com.apiservices.core.entity.ClienteEntity;

import java.util.List;

public interface ClienteGateway {

    ClienteEntity salvar(ClienteEntity cliente);
    ClienteEntity ataulizarCliente(ClienteEntity entity, Long id);
    void deleteById(Long id);
    List<ClienteEntity> obterClientes();
    ClienteEntity obterClientePorId(Long id);
}
