package com.apiservices.core.usecase;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.core.gateway.ClienteGateway;
import com.apiservices.dataprovider.ClienteDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class CadastroClienteUseCase {

	private final static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now();
	private static final Logger logger = LoggerFactory.getLogger(ClienteDataProvider.class);

	private ClienteGateway clienteGateway;

	@Autowired
	public CadastroClienteUseCase(ClienteGateway clienteGateway) {
		this.clienteGateway = clienteGateway;
	}

	public ClienteEntity salvar(ClienteEntity cliente) {
		cliente.setDataHoraCriacao(LOCAL_DATE_TIME);
		return clienteGateway.salvar(cliente);
	}
	
	public void excluir(Long id) {
		clienteGateway.deleteById(id);
	}

	public List<ClienteEntity> obterClientes() {
		return clienteGateway.obterClientes();
	}

	public ClienteEntity findById(Long id) {
		return clienteGateway.obterClientePorId(id);
	}

	public ClienteEntity ataulizarCliente(ClienteEntity entity, Long id) {
		return clienteGateway.ataulizarCliente(entity,id);
	}

	public void validaCliente(Long id){
		if (clienteGateway.obterClientePorId(id) == null){
			logger.info("[Cliente] | Cliente não existe no banco de dados");
			throw new RuntimeException("Cliente não existe no banco de dados");
		}
	}
}
