package com.apiservices.dataprovider.mappers;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.dataprovider.entity.ClienteTable;

public class ClienteTableTesteMapper {

    public static ClienteTable toTable(ClienteEntity entity) {

        return ClienteTable.builder()
                .id(entity.getId())
                .nome(entity.getNome())
                .email(entity.getEmail())
                .telefone(entity.getTelefone())
                .dataHoraCriacao(entity.getDataHoraCriacao())
                .dataHoraAlteracao(entity.getDataHoraAlteracao())
                .build();
    }

    public static ClienteEntity toEntity(ClienteTable table) {

        return ClienteEntity.builder()
                .id(table.getId())
                .nome(table.getNome())
                .email(table.getEmail())
                .telefone(table.getTelefone())
                .dataHoraCriacao(table.getDataHoraCriacao())
                .dataHoraAlteracao(table.getDataHoraAlteracao())
                .build();
    }
}
