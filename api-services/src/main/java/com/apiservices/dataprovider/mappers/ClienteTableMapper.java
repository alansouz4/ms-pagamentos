package com.apiservices.dataprovider.mappers;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.dataprovider.entity.ClienteTable;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClienteTableMapper {

    ClienteTableMapper INSTANCE = Mappers.getMapper(ClienteTableMapper.class);

    ClienteTable entityToTable(ClienteEntity clienteEntity);

    ClienteEntity tableToEntity(ClienteTable table);
}
