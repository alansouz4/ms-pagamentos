package com.apiservices.dataprovider.repository;

import com.apiservices.dataprovider.entity.ClienteTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<ClienteTable, Long>{

}
