package com.msoauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *  EndPoint
 *  Configura a Autenticação, quando a Aplicação recebe as credenciais do Usuário
 * e chama-o no banco do micro-service de User, compara a senha pra autenticar e
 * devolver o token.
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private BCryptPasswordEncoder cryptPasswordEncoder;
    private UserDetailsService userDetailsService;

    @Autowired
    public SecurityConfig(BCryptPasswordEncoder cryptPasswordEncoder, UserDetailsService userDetailsService) {
        this.cryptPasswordEncoder = cryptPasswordEncoder;
        this.userDetailsService = userDetailsService;
    }

    /**
     * Recebe o UserDetais com as credenciais e o hash da senha
     *
     * @param auth
     * @throws Exception
     */
    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(cryptPasswordEncoder);
    }

    /**
     * Injetanto o AuthenticationManager
     *
     * @return
     * @throws Exception
     */
    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }


}
