package com.msoauth.feignclients;

import com.msoauth.entities.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "ms-user", path = "/v1/users", configuration = UserFeignClientConfiguration.class)
public interface UserFeignClients {

    @GetMapping("/search")
    ResponseEntity<User> obterClientePorEmail(@RequestParam String email);

}
