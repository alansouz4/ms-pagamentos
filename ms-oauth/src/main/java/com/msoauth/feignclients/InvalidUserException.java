package com.msoauth.feignclients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O usuário informado é inválido")
public class InvalidUserException extends RuntimeException{
}
