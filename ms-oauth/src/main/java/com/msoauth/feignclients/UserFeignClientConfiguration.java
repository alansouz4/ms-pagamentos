package com.msoauth.feignclients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class UserFeignClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new UserFeignClientDecoder();
    }
}
