package com.msoauth.feignclients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class UserFeignClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {

        if (response.status() == 404) {
            return new InvalidUserException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
