package com.msoauth.usecase;

import com.msoauth.entities.User;
import com.msoauth.feignclients.UserFeignClients;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserUseCase implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(UserUseCase.class);

    private UserFeignClients userFeignClients;


    @Autowired
    public UserUseCase(UserFeignClients userFeignClients) {
        this.userFeignClients = userFeignClients;
    }

    @HystrixCommand(fallbackMethod = "obterUserPorEmailAlternativo")
    public User obterUserPorEmail(String email){
        User user = userFeignClients.obterClientePorEmail(email).getBody();
        if (user == null) {
            logger.error("Email não encontrado " + email);
            throw new IllegalStateException("Email não existe");
        }
        logger.info("Email encontrato " + email);
        return user;
    }

    public User obterUserPorEmailAlternativo(String email){
        User user = User.builder().email("nina@gmail.com").build();
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userFeignClients.obterClientePorEmail(username).getBody();
        if (user == null) {
            logger.error("Email não encontrado " + username);
            throw new UsernameNotFoundException("Email não existe");
        }
        logger.info("Email encontrato " + username);
        return user;
    }
}
