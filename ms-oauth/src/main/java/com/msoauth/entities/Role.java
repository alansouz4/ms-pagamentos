package com.msoauth.entities;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String roleName;
}
