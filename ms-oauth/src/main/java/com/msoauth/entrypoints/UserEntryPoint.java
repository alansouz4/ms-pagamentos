package com.msoauth.entrypoints;

import com.msoauth.entities.User;
import com.msoauth.usecase.UserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/users")
public class UserEntryPoint {

    private UserUseCase userUseCase;

    @Autowired
    public UserEntryPoint(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @GetMapping(value = "/search")
    public ResponseEntity<User> findByEmail(@RequestParam String email){
        try {
            User user = userUseCase.obterUserPorEmail(email);
            return ResponseEntity.ok(user);
        } catch (IllegalArgumentException e ) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }
}
